# https://www.gnu.org/software/make/manual/make.html
SHELL=bash
.PHONY: default
default:

py_source=./src ./tests

.PHONY: toolchain
toolchain:
	go install github.com/tomwright/dasel/cmd/dasel@v1.20.0

.PHONY: update-latest
update-latest:
	dasel select -f pyproject.toml -m 'tool.poetry.dev-dependencies.-' \
		| sed 's/.*/&@latest/g' \
		| xargs -n1 poetry add --dev
	dasel select -f pyproject.toml -m 'tool.poetry.dependencies.-' \
		| grep -v '^python' \
		| sed 's/.*/&@latest/g' \
		| xargs -n1 poetry add

.PHONY: test
test:
	poetry run pytest --cov-config=pyproject.toml --cov-report term --cov-report xml --cov=src ./tests

.PHONY: test-debug
test-debug:
	poetry run pytest -rA --cov-config=pyproject.toml --cov-report term --cov-report xml --cov=src ./tests

.PHONY: validate-static
validate-static:
	poetry run mypy --show-error-codes --show-error-context \
		$(py_source)
	poetry run codespell $(py_source)
	poetry run isort --check $(py_source)
	poetry run black --check $(py_source)
	poetry run flake8 $(py_source)
	poetry export --without-hashes --dev --format requirements.txt | poetry run safety check --full-report --stdin


.PHONY: validate
validate: validate-static test
default: validate

.PHONY: validate-fix
validate-fix:
	poetry run isort $(py_source)
	poetry run black $(py_source)

.PHONY: install-editable
install-editable:
	rm -rv src/*.egg-info/ || :
	## uninstall
	pip3 uninstall -y "$$(poetry version | gawk '{ print $$1 }')"
	## install
	rm -rv dist/ || :
	poetry build --format sdist \
		&& tar --wildcards -xvf dist/*.tar.gz -O '*/setup.py' > setup.py \
		&& pip3 install --prefix="$${HOME}/.local/" --editable .

clean: clean-dist/

.PHONY: install-user
install-user:
	rm -rv src/*.egg-info/ || :
	## uninstall
	pip3 uninstall -y "$$(poetry version | gawk '{ print $$1 }')"
	## install
	rm -rv dist/ || :
	poetry build --format sdist \
		&& pip3 install --prefix="$${HOME}/.local/" dist/*.tar.gz

help:
	@printf "########################################################################\n"
	@printf "TARGETS:\n"
	@printf "########################################################################\n"
	@printf "%-32s%s\n" "help" "Show this output ..."
	@printf "%-32s%s\n" "validate" "Validate everything"
	@printf "%-32s%s\n" "validate-fix" "Fix auto-fixable validation errors"
	@printf "########################################################################\n"

.PHONY: clean-%/
clean-%/:
	@{ test -d $(*) && { set -x; rm -vr $(*); set +x; } } || echo "directory $(*) does not exist ... nothing to clean"
