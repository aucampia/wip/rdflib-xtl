import typing as typ

from aucampia.rdflib_xtl import cli


def test_sparql(capsys: typ.Any) -> None:
    rc = cli.main(
        standalone_mode=False,
        args=[
            "sparql",
            "--query",
            "SELECT * WHERE { ?s rdfs:subClassOf owl:Thing. }",
            "http://xmlns.com/foaf/spec/index.rdf",
        ],
    )
    assert rc is None
    captured = capsys.readouterr()
    assert "http://xmlns.com/foaf/0.1/OnlineAccount" in captured.out.splitlines()


def test_sparql_reason(capsys: typ.Any) -> None:
    rc = cli.main(
        standalone_mode=False,
        args=[
            "sparql",
            "--query",
            "SELECT * WHERE { ?s rdfs:subClassOf <http://xmlns.com/foaf/0.1/Image> }",
            "--reason",
            "http://xmlns.com/foaf/spec/index.rdf",
        ],
    )
    assert rc is None
    captured = capsys.readouterr()
    assert len(captured.out.splitlines()) > 1


def test_reason(capsys: typ.Any) -> None:
    rc = cli.main(
        standalone_mode=False, args=["reason", "http://xmlns.com/foaf/spec/index.rdf"]
    )
    assert rc is None
    captured = capsys.readouterr()
    found = False
    for line in captured.out.splitlines():
        if line.startswith("owl:Ontology"):
            found = True
    assert found
